<!DOCTYPE html>
<html lang="en">

<head>
    <title>SF - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('/studylab/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('/studylab/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('/studylab/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('/studylab/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/style.css')}}">
    @stack('styles')
</head>

<body>
    @include('partials/nav')
    <!-- END nav -->

    <section class="hero-wrap" style="background-image: url('images/bg_2.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="col-md-9 ftco-animate pb-5 text-center">
                <p class="breadcrumbs">

                </p>
                <!-- <h1 class="mb-0 bread">Course Lists</h1> -->
            </div>
        </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row">
                @include('partials/sidebar')

                <!-- konten ada disini -->
                <div class="col-lg-9">
                    <div class="sidebar-box bg-white p-4 ftco-animate">
                        <a href="#">
                            <h3 class="heading-sidebar">@yield('subtitle') </h3>
                        </a>
                    </div>
                    <div class="sidebar-box bg-white p-4 ftco-animate">
                        @yield('content')
                    </div>
                </div>
            </div>
    </section>

    @include('partials/footer')

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>

    <script src="{{asset('/studylab/js/jquery.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('/studylab/js/popper.min.js')}}"></script>
    <script src="{{asset('/studylab/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('/studylab/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('/studylab/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('/studylab/js/scrollax.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="{{asset('/studylab/js/google-map.js')}}"></script>
    <script src="{{asset('/studylab/js/main.js')}}"></script>
    @include('sweetalert::alert')
    @stack('script')
    @stack('scripts')
</body>

</html>