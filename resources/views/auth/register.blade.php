@extends('layout.auten')

@section('subtitle')
{{ __('Register') }} Sanberforum
@endsection

@section('content')
<p class="text-center">Sudah memiliki akun? <a href="/login">Login</a></p>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="p-4 p-md-5">
                <form method="POST" action="{{ route('register') }}" class="signup-form">
                    @csrf
                    <div class="form-group">
                        <label class="label" for="name">{{ __('Name') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                    <div class="form-group">
                        <label class="label" for="umur">Umur</label>
                        <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ old('umur') }}">

                        @error('umur')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label" for="email">{{ __('Email Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label" for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                    </div>

                    <div class="form-group">
                        <label class="label">Alamat</label>
                        <textarea name="alamat" class="form-control" id=""></textarea>
                    </div>
                    @error('alamat')
                    <strong>{{ $message }}</strong>
                    @enderror

                    <div class="form-group">
                        <label class="label">Bio</label>

                        <textarea name="bio" class="form-control" id=""></textarea>
                    </div>
                    @error('bio')
                    <strong>{{ $message }}</strong>
                    @enderror


                    <div class="form-group d-flex justify-content-end mt-4">
                        <button type="submit" class="btn btn-primary submit"><span class="fa fa-paper-plane"></span> {{ __('Register') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection