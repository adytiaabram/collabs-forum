@extends('layout.auten')
@section('title')
Halaman Member
@endsection


@section('content')
<h2 class="mb-4 text-center">Member</h2>
<div>
    <div class="media my-3 justify-content-center" data-spy="scroll" data-offset="0">

        <div class="col-md-6 col-lg-3 ftco-animate d-flex align-items-stretch">
            <div class="staff">
                <div class="img-wrap d-flex align-items-stretch">
                    <div class="img align-self-stretch" style="background-image: url(image/jerryness.jpg);"></div>
                </div>
                <div class="text pt-3">
                    <h3><a href="instructor-details.html">I Kadek Jerry Septiawan</a></h3>
                    <span class="position mb-2">@jerryness</span>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate d-flex align-items-stretch">
            <div class="staff">
                <div class="img-wrap d-flex align-items-stretch">
                    <div class="img align-self-stretch" style="background-image: url(image/bramlicious.jpg);"></div>
                </div>
                <div class="text pt-3">
                    <h3><a href="instructor-details.html">Adytia Abram Tambunan</a></h3>
                    <span class="position mb-2">@bramlicious</span>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate d-flex align-items-stretch">
            <div class="staff">
                <div class="img-wrap d-flex align-items-stretch">
                    <div class="img align-self-stretch" style="background-image: url(image/dhikasyahriza.jpg);"></div>
                </div>
                <div class="text pt-3">
                    <h3><a href="instructor-details.html">Muhammad Dhika Syahriza</a></h3>
                    <span class="position mb-2">@dhikasyahriza</span>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection