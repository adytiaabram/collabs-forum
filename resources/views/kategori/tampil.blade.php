@extends('layout/master')

@section('title') Halaman Kategori @endsection

@section('subtitle')
Data Kategori
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.1/datatables.min.js"></script>
<script>
    $(document).ready(function() {
        $("#mykategori").DataTable();
    });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.1/datatables.min.css" />
@endpush

@section('content')
@auth
<a href="/kategori/create" class="btn btn-sm btn-primary mb-3">Tambah</a>
@endauth
<table id="mykategori" class="table">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            @auth
            <th scope="col">Action</th>
            @endauth
        </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key => $item)
        <tr>
            <td>
                {{$key+1}}
            </td>
            <td>
                {{$item->nama}}
            </td>
            @auth

            <td>
                <form action="/kategori/{{$item->id}}" method="post">
                    @method('delete')
                    @csrf
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                </form>
            </td>
            @endauth
        </tr>

        @empty
        <tr>
            <td colspan="4">
                Tidak ada data
            </td>
        </tr>
        @endforelse
    </tbody>
</table>



@endsection