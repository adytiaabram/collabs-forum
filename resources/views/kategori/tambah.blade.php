@extends('layout/master')
@section('title') Halaman Kategori @endsection

@section('subtitle')
Data Kategori
@endsection

@section('content')

<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Masukkan nama kategori">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection
