@extends('layout/master')

@section('title') Halaman Kategori @endsection

@section('subtitle')
Data Kategori
@endsection

@section('content')
<!-- <h3>Tambah Data Cast</h3> -->
<form action="/kategori/{{$kategori->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{$kategori->nama}}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection