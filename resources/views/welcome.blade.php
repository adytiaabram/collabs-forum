<!DOCTYPE html>
<html lang="en">

<head>
    <title>StudyLab - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('/studylab/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('/studylab/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('/studylab/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('/studylab/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('/studylab/css/style.css')}}">
</head>

<body>
    @include('partials/nav')
    <!-- END nav -->

    <div class="hero-wrap js-fullheight" style="background-image: url({{ asset('/studylab/images/bg_1.jpg') }})">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center" data-scrollax-parent="true">
                <div class="col-md-7 ftco-animate">
                    <span class="subheading">Selamat datang di Sanberforum</span>
                    <h1 class="mb-4">Tak Usah Khawatir Saat Mengalami Kendala dalam Belajar</h1>
                    <p class="caps">Tanyakan kendala belajarmu di Forum ini</p>
                    <p class="mb-0">
                        <a href="{{ route('register') }}" class="btn btn-primary">Register</a>
                        <a href="/pertanyaan" class="btn btn-white">Lihat Diskusi</a>

                        <!-- <a href="{{ route('register') }}" class="btn btn-info">Register</a> -->
                    </p>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @guest
    <section class="ftco-section ftco-no-pb ftco-no-pt">
        <div class="container">
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5 order-md-last">
                    <div class="login-wrap p-4 p-md-5">
                        <h3 class="mb-4">Gabung Sekarang</h3>
                        <form action="/register" class="signup-form">
                            <div class="form-group">
                                <label class="label" for="name">Nama lengkap</label>
                                <input type="text" class="form-control" placeholder="John Doe">
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">Alamat email</label>
                                <input type="text" class="form-control" placeholder="johndoe@gmail.com">
                            </div>
                            <div class="form-group d-flex justify-content-end mt-4">
                                <a href="/register" class="btn btn-primary submit"><span class="fa fa-paper-plane"></span> Register</a>
                                <!-- <button type="submit" class="btn btn-primary submit"><span class="fa fa-paper-plane"></span></button> -->
                            </div>
                        </form>
                        <p class="text-center">Sudah memiliki akun? <a href="/login">Login</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endguest

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center pb-4">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <!-- <span class="subheading">Mulai Diskusi</span> -->
                    <h2 class="mb-4">Kategori Diskusi</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                @forelse ($kategori->slice(0, 5) as $itemss)
                <div class="col-md-3 col-lg-2">
                    <a href="/pertanyaan/" class="course-category img d-flex align-items-center justify-content-center" style="background-color: #5f8ff0   ;">
                        <div class="text w-100 text-center">
                            <h3>{{$itemss->nama}}</h3>
                            <!-- <span>100 course</span> -->
                        </div>
                    </a>
                </div>
                @empty

                <div class="col-md-12 text-center mt-5">
                    <p class="text-center"> Tidak ada Kategori </p>
                    @auth
                    <a href="/kategori/create" class="btn btn-secondary">Buat Kategori</a>
                    @endauth
                </div>
                @endforelse

            </div>
        </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center pb-4">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <!-- <span class="subheading">Diskusi</span> -->
                    <h2 class="mb-4">Forum Diskusi</h2>
                </div>
            </div>
            <div class="row d-flex">

                @forelse ($pertanyaan->slice(0, 3) as $item)

                <div class="col-lg-4 ftco-animate">
                    <div class="blog-entry">
                        <a href="/pertanyaan/{{$item->id}}">
                            <img class=" img -fluid img thumbnail" style="object-fit: cover; width: 100%;" src="{{asset('/image/'.$item->gambar)}}" alt="Generic placeholder image" height="300px">
                        </a>
                        <div class="text d-block">
                            <div class="meta">
                                <p class=" d-flex justify-content-between">
                                    <a href="#"><span class="fa fa-list mr-2"></span> {{$item->kategori->nama}} </a>
                                    <a href="#"> <span class="fa fa-user mr-2"></span> {{$item->user->name}} </a>
                                    <!-- <a href="#" class="meta-chat"><span class="fa fa-comment mr-2"></span> 3</a> -->
                                </p>
                            </div>
                            <h3 class="heading"><a href="#">{{$item->judul}}</a></h3>
                            {{-- <p class="card-text">{{Str::limit($item->content, 50)}}</p> --}}
                            {{-- tinymce --}}
                            <p class="card-text">{!! Str::limit(strip_tags($item->content), 50) !!}</p>
                            <!-- <p>{{Str::limit($item->content, 50)}}</p> -->
                            <p><a href="/pertanyaan/{{$item->id}}" class="btn btn-sm btn-secondary py-2 px-3">Read more</a></p>
                        </div>
                    </div>
                </div>


                @empty

                <div class="col-md-12 text-center mt-5">
                    <p class="text-center"> Tidak ada Postingan </p>
                    @auth
                    <a href="/pertanyaan/create" class="btn btn-secondary">Buat Postingan</a>
                    @endauth
                </div>

                @endforelse

                <div class="col-md-12 text-center mt-5">
                    <a href="/pertanyaan" class="btn btn-secondary">Lihat semua diskusi</a>
                </div>


            </div>

        </div>
    </section>


    @include('partials/footer')



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>


    <script src="{{asset('/studylab/js/jquery.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('/studylab/js/popper.min.js')}}"></script>
    <script src="{{asset('/studylab/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('/studylab/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('/studylab/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('/studylab/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('/studylab/js/scrollax.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="{{asset('/studylab/js/google-map.js')}}"></script>
    <script src="{{asset('/studylab/js/main.js')}}"></script>

</body>

</html>