@extends('layout.auten')

@section('subtitle')
{{ __('Edit') }} Biodata
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="p-4 p-md-5">

                <form method="POST" action="/profile/{{$profile->id}}" class="signup-form">
                    @csrf
                    @method('put')
                     
                    {{-- Read Only --}}
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input class="form-control" type="text" placeholder="{{$profile->user->name}}" readonly>
                    </div>

                    <div class="form-group">
                        <label for="name">E-mail</label>
                        <input class="form-control" type="text" placeholder="{{$profile->user->email}}" readonly>
                    </div>
                    
                    {{-- Can Edit --}}
                    <div class="form-group">
                        <label class="label" for="umur">Umur</label>
                        <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$profile->umur}}">

                        @error('umur')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label">Alamat</label>
                        <textarea name="alamat" class="form-control" id="">{{$profile->alamat}}</textarea>
                    </div>
                    @error('alamat')
                    <strong>{{ $message }}</strong>
                    @enderror

                    <div class="form-group">
                        <label class="label">Bio</label>

                        <textarea name="bio" class="form-control" id="">{{$profile->bio}}</textarea>
                    </div>
                    @error('bio')
                    <strong>{{ $message }}</strong>
                    @enderror


                    <div class="form-group d-flex justify-content-end mt-4">
                        <button type="submit" class="btn btn-primary submit"><span class="fa fa-paper-plane"></span> {{ __('Submit') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection