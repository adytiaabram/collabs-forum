@extends('layout.master')
@section('title')
Halaman Pertanyaan
@endsection

@section('subtitle')
Pertanyaan : {{$pertanyaan->judul}}
@endsection

@section('content')

<div class="project-wrap">
  <div class="text p-4">
    <img class="media-img-top" src="{{asset('/image/'.$pertanyaan->gambar)}}" style="object-fit: cover; width: 60%;" alt="Media image cap">
  </div>
  <div class="text p-4">
    <p class="advisor">Oleh <span>{{$pertanyaan->user->name}}</span></p>
    <ul class="d-flex justify-content-between">
      <span class="flaticon-shower"></span>{!!$pertanyaan->content!!}
      <!-- <li class="price">$199</li> -->
    </ul>
  </div>
</div>

<ul class="d-flex justify-content-between">
  Jawaban
  <li>total jawaban</li>
</ul>
@forelse ($pertanyaan->jawaban as $item)
<div class="project-wrap">
  <div class="about-author d-flex p-4 bg-light">
    <div class="col-5">
      <img src="{{asset('/jawaban/'.$item->gambar)}}" style="object-fit: contain;" alt="Image placeholder" class="img-fluid">
    </div>
    <div class="col-7 ">
      <div class="d-flex justify-content-between text-primary ">
        <h3>{{$item->user->name}}</h3>
        <p> Liked: 100</p>
      </div>
      <div>
        <p>{{$item->jawaban}}</p>
      </div>
      <form action="/jawaban/{{$item->id}}" method="POST">
        @csrf
        @method('delete')
        @if(Auth::id() == $item->user_id)
        <a href="/jawaban/{{$item->id}}/edit" class="btn btn-sm btn-info mr-1">Edit</a>
        <input type="submit" class="btn btn-sm btn-danger mr-1" value="Delete">
       
        @endif
      </form> 
      
      
      
      
      <!-- <div class="row"> -->
     
    </div>
  </div>
  </div>

@empty
<h4>Tidak Ada jawaban</h4>
@endforelse

</hr>


@auth
<form action="/jawaban/{{$pertanyaan->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  <textarea name="jawaban" cols="10" row="30" class="form-control my-3" placeholder="Tambah Jawaban"></textarea>
  @error('jawaban')
  <div class="alert alert-danger">
    {{$message}}
  </div>
  @enderror
  <div class="form-group">
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control">
  </div>
  @error('gambar')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="submit" class="btn btn-primary btn-sm" value="Kirim Jawaban">
</form>
@endauth
<!-- <a href="/pertanyaan" class="btn btn-primary btn-block btn-sm">Kembali</a> -->
@endsection
