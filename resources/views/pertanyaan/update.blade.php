@extends('layout.master')
@section('title')
    Halaman Pertanyaan
@endsection

@section('subtitle')
Pertanyaan anda
@endsection


@section('content')

<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{$pertanyaan->judul}}" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content</label>
        <textarea name="content"  class="form-control" id=""  rows="30" cols="10" > {{$pertanyaan->content}}</textarea>
      </div>
      @error('content')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control">
      </div>
      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            @forelse ($kategori as $item)
            @if ($item->id===$pertanyaan->kategori_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>                    
            @endif
                
            @empty
                <option value="">Tidak ada data kategori</option>
            @endforelse
        </select>
      </div>
      @error('kategori_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  {{-- tinymce update pertanyaan --}}
  @push('script')
  <script src="https://cdn.tiny.cloud/1/6zdta66k3p4jkjaw8bdvu9cbtj5giatoa0zd958iqog7ady4/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
      tinymce.init({
        selector: 'textarea',
        plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        mergetags_list: [
          { value: 'First.Name', title: 'First Name' },
          { value: 'Email', title: 'Email' },
        ]
      });
    </script>
  @endpush


@endsection