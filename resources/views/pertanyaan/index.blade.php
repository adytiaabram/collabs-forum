@extends('layout.master')
@section('title')
Halaman Pertanyaan
@endsection

@section('subtitle')
Punya Pertanyaan? Yuk posting disini
@endsection


@section('content')

@auth
<a href="/pertanyaan/create" class="btn btn-primary btn-sm mb-4">Buat Pertanyaan</a>
@endauth

@forelse ($pertanyaan as $item)

<div class="project-wrap">
    <div class="media my-3" data-spy="scroll" data-offset="0">
        <div class="media-body">
            <a href="/pertanyaan/{{$item->id}}">
                <img class=" img -fluid img thumbnail" style="object-fit: cover; width: 90%;" src="{{asset('/image/'.$item->gambar)}}" alt="Generic placeholder image" width="50px">
            </a>
        </div>
        <div class="media-body ml-4">
            <div>
                <span class="badge badge-info mr-5"> Pertanyaan oleh : {{$item->user->name}}</span>
            </div>
            <div>
                <span class="badge badge-warning mr-5"> Kategori : {{$item->kategori->nama}}</span>
            </div>

            <h5 class="mt-0">{{$item->judul}}</h5>
            {{-- <p class="card-text">{{Str::limit($item->content, 50)}}</p> --}}
            {{-- tinymce --}}
            <p class="card-text">{!! Str::limit(strip_tags($item->content), 50) !!}</p>

            @auth
            <!-- <div class="row"> -->
            <form action="/pertanyaan/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/pertanyaan/{{$item->id}}" class="btn btn-sm btn-primary mr-1">Detail</a>
                @if(Auth::id() == $item->user_id)
                <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-sm btn-info mr-1">Edit</a>
                <input type="submit" class="btn btn-sm btn-danger mr-1" value="Delete">
                @endif



            </form>
            <!-- </div> -->
            @endauth

        </div>
    </div>
</div>
@empty
<p class="text-center"> Tidak ada Postingan </p>
@endforelse


@endsection