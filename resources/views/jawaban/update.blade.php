@extends('layout.master')
@section('title')
    Halaman Edit jawaban
@endsection

@section('subtitle')
Jawaban anda
@endsection


@section('content')

<form action="/jawaban/{{$jawaban->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Jawaban</label>
      <input type="text" name="jawaban" value="{{$jawaban->jawaban}}" class="form-control">
    </div>
    @error('jawaban')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control">
      </div>
      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection