<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container ">


        <div class="collapse navbar-collapse" id="ftco-nav">
            <a class="navbar-brand" href="/"><span>Sanber</span>Forum</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="/pertanyaan" class="nav-link">Diskusi</a></li>
                <li class="nav-item"><a href="/member" class="nav-link">Member</a></li>
                <!-- <li class="nav-item"><a href="{{ route('register') }}" class="nav-link">Register</a></li> -->
                @guest
                <li class="nav-item bg-info">
                    <a href="/login" class="nav-link">
                        <p>
                            Login
                        </p>
                    </a>
                </li>
                @endguest
                @auth
                <li class="nav-item btn btn-secondary btn-lg">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>

                @endauth
            </ul>
        </div>
    </div>
</nav>