<div class="col-lg-3 sidebar">
    {{-- Penambahan Info yang login --}}
    <div class="info">
        @auth
        <a href="/profile" class="sidebar-box btn btn-light ftco-animate">
            <h3 class="heading-sidebar">{{ Auth::user()->name }} ({{Auth::user()->profile->umur}})</h3>
        </a>
        @endauth
        @guest
        <a href="/login" class="sidebar-box btn btn-light ftco-animate">Anda Belum Login</a>
        @endguest

    </div>
    <!-- <div class="sidebar-box bg-white ftco-animate">
        <form action="#" class="search-form">
            <div class="form-group">
                <span class="icon fa fa-search"></span>
                <input type="text" class="form-control" placeholder="Search...">
            </div>
        </form>
    </div> -->
    @auth
    <a href="/pertanyaan" class="sidebar-box btn btn-light ftco-animate">
        <h3 class="heading-sidebar">Diskusi</h3>
    </a>
    @endauth
    <a href="/" class="sidebar-box btn btn-light ftco-animate">
        <h3 class="heading-sidebar">Dashboard</h3>
    </a>
    <a href="/kategori" class=" sidebar-box btn btn-light ftco-animate">
        <h3 class="heading-sidebar">Kategori</h3>
    </a>

    <!-- @auth
    <a href="/profile" class="sidebar-box btn btn-light ftco-animate">
        <h3 class="heading-sidebar">Profile</h3>
    </a>
    @endauth -->




    <!-- <div class="sidebar-box bg-white p-4 ftco-animate">
        <h3 class="heading-sidebar">Kategori</h3>
        <form action="#" class="browse-form">
            <label for="option-software-1"><input type="checkbox" id="option-software-1" name="vehicle" value="" checked> Adobe Photoshop</label><br>
            <label for="option-software-2"><input type="checkbox" id="option-software-2" name="vehicle" value=""> Adobe Illustrator</label><br>
            <label for="option-software-3"><input type="checkbox" id="option-software-3" name="vehicle" value=""> Sketch</label><br>
            <label for="option-software-4"><input type="checkbox" id="option-software-4" name="vehicle" value=""> WordPress</label><br>
            <label for="option-software-5"><input type="checkbox" id="option-software-5" name="vehicle" value=""> HTML &amp; CSS</label><br>
        </form>
    </div> -->
</div>