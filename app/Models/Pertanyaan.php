<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;
    protected $table = 'pertanyaan';
    protected $fillable = ['judul','content','gambar','kategori_id','user_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function kategori(){
        return $this->belongsTo(kategori::class,'kategori_id');
    }
    public function jawaban()
    {
        return $this->hasMany(jawaban::class,('pertanyaan_id'));
    }
}
