<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Jawaban;
use App\Models\Pertanyaan;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use File;

class JawabanController extends Controller
{
    public function tambah(request $request, $id)
    {
        $request->validate([
            'jawaban' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        //upload foto 
        $filename = time() . '.' . $request->gambar->extension();
        $request->gambar->move(public_path('jawaban'), $filename);

        $iduser = Auth::id();
        //upload database
        $jawaban = new Jawaban;

        $jawaban->user_id = $iduser;
        $jawaban->pertanyaan_id = $id;
        $jawaban->jawaban = $request->jawaban;
        $jawaban->gambar = $filename;

        $jawaban->save();
        Alert::success('BERHASIL', 'Jawaban Masuk');
        return redirect('/pertanyaan/'.$id)->with('id', $id);
    }
        public function edit($id)
        {
            $jawaban = Jawaban::find($id);

            return view('jawaban.update', ['jawaban' => $jawaban]);
        }
        public function update(Request $request, $id)
        {
            $request->validate([
                'jawaban' => 'required',
                'gambar' => 'image|mimes:jpeg,png,jpg'
            ]);
    
            $jawaban = Jawaban::find($id);

            if ($request->has('gambar')) {
                $path = 'jawaban/';
                File::delete($path . $jawaban->gambar);
    
                $filename = time() . '.' . $request->gambar->extension();
                $request->gambar->move(public_path('jawaban'), $filename);
                $jawaban->gambar = $filename;
                $jawaban->save();
            }

            $jawaban->jawaban = $request['jawaban'];
            $jawaban->save();
            $idper= $jawaban->pertanyaan_id;
            Alert::success('BERHASIL', 'Mengubah jawaban');
            return redirect('/pertanyaan/'.$idper);
        }

        public function destroy($id)
        {
            
        $jawaban = Jawaban::find($id);
        
        $path = 'image/';
        File::delete($path . $jawaban->gambar);
        $jawaban->delete();
        $idper= $jawaban->pertanyaan_id;
        Alert::success('BERHASIL', 'Menghapus Jawaban');
        return redirect('/pertanyaan/'.$idper);    
        }

   
    
}
