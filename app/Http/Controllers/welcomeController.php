<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;
use App\Models\Kategori;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class welcomeController extends Controller
{
    public function index()
    {
        $pertanyaan = Pertanyaan::orderBy('id', 'DESC')->get();
        $kategori = Kategori::get();
        return view('welcome', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }
}
