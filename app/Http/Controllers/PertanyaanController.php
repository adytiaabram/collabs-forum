<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use File;
use RealRashid\SweetAlert\Facades\Alert;



class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::orderBy('id', 'DESC')->get();
        $kategori = Kategori::get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('pertanyaan.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        //upload foto 
        $filename = time() . '.' . $request->gambar->extension();
        $request->gambar->move(public_path('image'), $filename);

        $iduser = Auth::id();

        //pertanyaan database
        $pertanyaan = new Pertanyaan;

        $pertanyaan->user_id = $iduser;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->content = $request->content;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->gambar = $filename;

        $pertanyaan->save();
        Alert::success('BERHASIL', 'Menambahkan pertanyaan');
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.detail', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = kategori::get();

        return view('pertanyaan.update', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg'
        ]);

        $pertanyaan = Pertanyaan::find($id);
        if ($request->has('gambar')) {
            $path = 'image/';
            File::delete($path . $pertanyaan->gambar);

            $filename = time() . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('image'), $filename);
            $pertanyaan->gambar = $filename;
            $pertanyaan->save();
        }
        $pertanyaan->judul = $request['judul'];
        $pertanyaan->content = $request['content'];
        $pertanyaan->kategori_id = $request['kategori_id'];
        $pertanyaan->save();
        Alert::success('BERHASIL', 'Mengubah pertanyaan');
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $path = 'image/';
        File::delete($path . $pertanyaan->gambar);
        $pertanyaan->delete();
        Alert::success('BERHASIL', 'Menghapus pertanyaan');
        return redirect('/pertanyaan');
    }
}
