<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class kategoriController extends Controller
{
    public function index()
    {
        $kategori = DB::table('kategori')->get();
        return view('kategori/tampil', ['kategori' => $kategori]);
    }

    public function create()
    {
        return view('kategori/tambah');
    }

    public function store(Request $request)
    {

        $validated = $request->validate(
            [
                //ini untuk validasi
                'nama' => 'required',
            ],
            [
                //ini untuk custom aler validasi
                'nama.required' => 'Nama wajib diisi',
            ]

        );
        DB::table('kategori')->insert([
            'nama' => $request['nama']
        ]);

        return redirect('/kategori');
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori/edit', ['kategori' => $kategori]);
    }

    public function update($id, Request $request)
    {
        $validated = $request->validate(
            [
                //ini untuk validasi
                'nama' => 'required',
            ],
            [
                //ini untuk custom aler validasi
                'nama.required' => 'Nama wajib diisi'
            ]

        );

        DB::table('kategori')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama']
                ]
            );

        return redirect('kategori');
    }

    public function destroy($id)
    {
        DB::table('kategori')->where('id', $id)->delete();

        return redirect('kategori');
    }
}
