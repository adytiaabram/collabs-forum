<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\kategoriController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\welcomeController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [welcomeController::class, 'index']);

//tampil
Route::get('/kategori', [kategoriController::class, 'index']);

//imput
Route::get('/kategori/create', [kategoriController::class, 'create']);

//save input
Route::post('/kategori', [kategoriController::class, 'store']);

//edit
Route::get('/kategori/{id}/edit', [kategoriController::class, 'edit']);

//edit save
Route::put('/kategori/{id}', [kategoriController::class, 'update']);

//edit save
Route::delete('/kategori/{id}', [kategoriController::class, 'destroy']);

Route::resource('/pertanyaan', PertanyaanController::class);

// sudah di push 
Route::post('/jawaban/{pertanyaan_id}', [JawabanController::class, 'tambah']);
//edit jawaban
Route::get('/jawaban/{pertanyaan_id}/edit', [JawabanController::class, 'edit']);
//update jawaban
Route::put('/jawaban/{pertanyaan_id}', [JawabanController::class, 'update']);
//hapus jawaban

Route::delete('/jawaban/{pertanyaan_id}', [JawabanController::class, 'destroy']);


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Eloquent Relationships Profile
Route::get('/profile', [ProfileController::class, 'index']);
Route::put('/profile/{id}', [ProfileController::class, 'update']);

//Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
//   \UniSharp\LaravelFilemanager\Lfm::routes();
//}); 

Route::get('/member', function () {
    return view('member');
});
