# Final Project
## Kelompok 7
## Anggota Kelompok

- Adytia Abram Tambunan (@adytiaabram)
- I Kadek Jerry Septiawan (@jerryness)
- Muhammad Dhika Syahriza (@dhikasyahriza)

## Tema Kelompok
Forum Diskusi

## ERD
![img](public/erd/erd.jpg)

## Link Video

- Link Video Aplikasi : https://youtu.be/hMFMWKU3xNY
- Link Deploy : http://sanber7.ultrateknologi.com/

